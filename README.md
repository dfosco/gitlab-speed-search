# GitLab Speed Search ⚡️

A list of Alfred web searches for quickly searching GitLab – and more!

🎥 [Watch an overview on Youtube](https://www.youtube.com/watch?v=-tt-U5DHTUc)

### What is this

[Alfred](https://www.alfredapp.com) is an app for MacOS that replaces the native Spotlight with an expanded UI that offers more advanced functionality. One of these features is [**creating custom web searches**](https://www.alfredapp.com/help/features/web-search), a powerful feature that allows you to more quickly search GitLab or any other site.

Alfred is a free app, with more advanced features [available as a paid upgrade](https://www.alfredapp.com/powerpack/), but all the web searches available here work with the free version!

Many of what you see here can be done directly in browsers (see [Searching GitLab like a pro](https://about.gitlab.com/handbook/tools-and-tips/searching/)), but with Alfred you can trigger these searches from anywhere!

### Some Cool Examples 🧊

This repo contains a list of searches setup for different GitLab needs, and will help you quickly search through:

- handbook
- docs
- issues
- merge requests
- milestones
- and much more!

![](/images/examples/handbook.gif)

<details>
  <summary>
    See more examples 👇
  </summary>
  
  ---

#### Handbook 📙

![](/images/examples/handbook.gif)

YES

#### Issue authors 🙋

![](/images/examples/issue-author.gif)

You know, to quickly find all _your_ stuff

#### Yes, Merge Requests! 🛠

![](/images/examples/mr-name.gif)

Go get 'em!!

#### Milestones 🗓

![](/images/examples/milestone.gif)

Where were we, again?

#### Product Teams 🌀

![](/images/examples/teams.gif)

Find teams by name or people in context of their team

#### GitLab People 🕺🕺

![](/images/examples/people.gif)

Or just find everyone by name!

#### Open Vacancies 📌

![](/images/examples/jobs.gif)

See who's hiring


</details>

### Getting Started

1. [Download and install Alfred](https://www.alfredapp.com) 
2. Find the search you want to add on the list below and click the link -- it will save the search in Alfred
3. That's it!

You will be asked to open the link in the Alfred application: once you accept, the search will be available on Alfred. If you want icons to show up for each search, you'll need to add them yourself one by one, though: see [icons folder](https://gitlab.com/dfosco/gitlab-speed-search/-/tree/main/images/icons) and instructions in the [Customization](https://gitlab.com/dfosco/gitlab-speed-search#customize-and-create-new-searches) section below.

**Searches with ⚠️ on the name only work natively on Google Chrome** because they rely on the experimental [Link to Text Fragment](https://web.dev/text-fragments) feature. There are add-ons for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/link-to-text-fragment) and [Edge](https://microsoftedge.microsoft.com/addons/detail/link-to-text-fragment/pmdldpbcbobaamgkpkghjigngamlolag) to enable this feature in those browsers. There is a [Safari plugin](https://apps.apple.com/us/app/link-to-text-fragment/id1532224396), but it's currently broken.

#### GitLab General

**If the tables below are not displaying links, check them out [on this page](https://gitlab.com/dfosco/gitlab-speed-search/-/blob/main/README.md)**

| Search | Trigger | Description |
| ------ | ------ | ------ |
| [Handbook (Google search)](alfred://customsearch/GitLab%20Handbook%20%28Google%29%20Search/gg/utf8/nospace/https%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3Dsite%253Aabout.gitlab.com%2Fhandbook%20%7Bquery%7D) | `gg` | Searches Handbook on Google search |
| [Handbook (Page search)](alfred://customsearch/GitLab%20Handbook%20Search/gh/utf8/nospace/https%3A%2F%2Fabout.gitlab.com%2Fhandbook%2F%23stq%3D%7Bquery%7D%26stp%3D1) | `gh` | Searches Handbook on embeded search |
| [GitLab Docs](alfred://customsearch/GitLab%20Docs/docs/utf8/nospace/https%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3Dsite%253Adocs.gitlab.com%20%7Bquery%7D) | `docs` | Searches Docs on Google search |
| ⚠️ [GitLab Teams Directory](alfred://customsearch/GitLab%20Teams%20Directory/team/utf8/nospace/https%3A%2F%2Fabout.gitlab.com%2Fhandbook%2Fproduct%2Fcategories%2F%23%3A~%3Atext%3D%7Bquery%7D) | `team` | Searches product team directory page |
| ⚠️ [GitLab Team Page](alfred://customsearch/GitLab%20Team%20Page/people/utf8/nospace/https%3A%2F%2Fabout.gitlab.com%2Fcompany%2Fteam%2F%23%3A~%3Atext%3D%7Bquery%7D) | `people` | Searches employee list by name |
| [GitLab Username](alfred://customsearch/GitLab%20Username/%40/utf8/nospace/https%3A%2F%2Fabout.gitlab.com%2Fcompany%2Fteam%2F%23%7Bquery%7D) | `@` | Searches employee list by @ `username` |
| ⚠️ [GitLab Careers Page](alfred://customsearch/GitLab%20Careers%20%28Public%29/jobs/utf8/nospace/https%3A%2F%2Fboards.greenhouse.io%2Fgitlab%2F%23%3A~%3Atext%3D%7Bquery%7D) | `jobs` | Searches public vacancies page |
| ⚠️ [GitLab Careers (Internal)](alfred://customsearch/GitLab%20Careers%20-%20Internal/greenhouse/utf8/nospace/https%3A%2F%2Fgitlab.greenhouse.io%2Finternal_job_board%2F%23%3A~%3Atext%3D%7Bquery%7D) | `greenhouse` | Searches open vacancies on Greenhouse |
| [GitLab Unfiltered Youtube Channel](alfred://customsearch/Youtube%20Unfiltered/yt/utf8/nospace/https%3A%2F%2Fwww.youtube.com%2Fc%2FGitLabUnfiltered%2Fsearch%3Fquery%3D%7Bquery%7D) | `yt` | Searches videos on GitLab Unfiltered Youtube channel |
| [GitLab Features](alfred://customsearch/New%20GitLab%20Features/feature/utf8/nospace/https%3A%2F%2Fgitlab-com.gitlab.io%2Fcs-tools%2Fgitlab-cs-tools%2Fwhat-is-new-since%2F%3Ftab%3Ddot-com%26textSearch%3D%7Bquery%7D) | `feature` | Searches for a GitLab feature name on _Release Feature & Deprecation Overview_ dashboard |
| **Bookmarks** | `bk` | Search the bookmarks on your browser of choice! See more on the [Bookmarks section](https://gitlab.com/dfosco/gitlab-speed-search#bookmark-feature). |


#### GitLab Application

| Search | Trigger | Description |
| ------ | ------ | ------ |
| [GitLab Issue](alfred://customsearch/GitLab%20Issue/issue/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fgitlab-org%2Fgitlab%2Fissues%2F%3Fscope%3Dall%26utf8%3D%E2%9C%93%26state%3Dopened%26search%3D%7Bquery%7D) | `issue` | Searches issues on GitLab repository |
| [GitLab Issue Author](alfred://customsearch/GitLab%20Issue%20Author/issue%20author/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fdashboard%2Fissues%3Fscope%3Dall%26utf8%3D%25E2%259C%2593%26state%3Dopened%26author_username%3D%7Bquery%7D) | `issue author` | Searches issues on GitLab repository by author (`username`) |
| [GitLab Issue Label](alfred://customsearch/GitLab%20Issue%20Label/issue%20label/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fgitlab-org%2Fgitlab%2F-%2Fissues%3Fscope%3Dall%26utf8%3D%25E2%259C%2593%26state%3Dopened%26label_name%5B%5D%3D%7Bquery%7D) | `issue label` | Searches issues on GitLab repository by label |
| [GitLab Merge Request](alfred://customsearch/GitLab%20MR/mr/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fdashboard%2Fmerge_requests%3Fscope%3Dall%26utf8%3D%25E2%259C%2593%26state%3Dopened%26search%3D%7Bquery%7D) | `mr` | Searches merge requests on GitLab repository |
| [GitLab Merge Request by author](alfred://customsearch/GitLab%20MR%20Author/mr%20author/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fdashboard%2Fmerge_requests%3Fscope%3Dall%26utf8%3D%25E2%259C%2593%26state%3Dopened%26author_username%3D%7Bquery%7D) | `mr author` | Searches merge requests on GitLab repository by author (`username`) |
| [GitLab Merge Request by reviewer](alfred://customsearch/MR%20Reviewer/mr%20reviewer/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fdashboard%2Fmerge_requests%3Fscope%3Dall%26state%3Dopened%26reviewer_username%3D%7Bquery%7D) | `mr reviewer` | Searches merge requests on GitLab repository by reviewer (`username`) |
| [GitLab Milestone](alfred://customsearch/GitLab%20Milestone/milestone/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fgroups%2Fgitlab-org%2F-%2Fmilestones%3Fsearch_title%3D%7Bquery%7D%26sort%3Ddue_date_asc%26state%3D%26utf8%3D%25E2%259C%2593) | `milestone` | Searches milestones on GitLab repository. Also works on partial entries: `14.` returns `14.0`, `14.1`, `14.2` etc |


#### Design & Code

| Search | Trigger | Description |
| ------ | ------ | ------ |
| [Pajamas Design System](alfred://customsearch/Pajamas%20Design%20System/pajamas/utf8/nospace/https%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3Dsite%253Adesign.gitlab.com%2520%7Bquery%7D) | `pajamas` | Searches Pajamas documentation on Google search |
| [Pajamas Design System (shortcut)](alfred://customsearch/Pajamas%20Design%20System/pajamas/utf8/nospace/https%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3Dsite%253Adesign.gitlab.com%2520%7Bquery%7D) | `ds` | Same as above, with shorter trigger |
| [Icons](alfred://customsearch/GitLab%20Icons/icon/utf8/nospace/https%3A%2F%2Fgitlab-org.gitlab.io%2Fgitlab-svgs%2F%3Fq%3D%7Bquery%7D) | `icon` | Search icons from gitlab-svgs |
| [Figma Files](alfred://customsearch/Figma%20File/ff/utf8/nospace/https%3A%2F%2Fwww.figma.com%2Ffiles%2F972612628770206748%2Fsearch%3Fmodel_type%3Dfiles%26q%3D%7Bquery%7D%26fuid%3D935818568768429669) | `ff` | Search Figma files on GitLab Figma team |
| [GitLab UI](alfred://customsearch/GitLab-UI%20Code%20Search/css/utf8/nospace/https%3A%2F%2Fgitlab.com%2Fsearch%3Futf8%3D%25E2%259C%2593%26search%3D%7Bquery%7D%26group_id%3D9970%26project_id%3D7071551%26scope%3D%26search_code%3Dtrue%26snippets%3Dfalse%26repository_ref%3Dmain%26nav_source%3Dnavbar) | `css` | Code search on GitLab-UI repository |
| ⚠️ [CSS Utils](alfred://customsearch/CSS%20Utils/utils/utf8/nospace/https%3A%2F%2Funpkg.com%2Fbrowse%2F%40gitlab%2Fui%2Fsrc%2Fscss%2Futilities.scss%23%3A~%3Atext%3D%7Bquery%7D) | `utils` | Search CSS utils on latest published source code – e.g.: `gl-ml-3` |
| ⚠️ [CSS Variables](alfred://customsearch/CSS%20Variables/var/utf8/nospace/https%3A%2F%2Funpkg.com%2Fbrowse%2F%40gitlab%2Fui%2Fsrc%2Fscss%2Fvariables.scss%23%3A~%3Atext%3D%7Bquery%7D) | `var` | Search CSS variables on latest published source code – e.g.: `gray-500` |

#### Random

| Search | Trigger | Description |
| ------ | ------ | ------ |
| [Giphy](alfred://customsearch/Giphy%20Search/gif/utf8/-/https%3A%2F%2Fgiphy.com%2Fsearch%2F%7Bquery%7D) | `gif` | Giphy search |
| [Gmail Search](alfred://customsearch/Gmail/gmail/utf8/nospace/https%3A%2F%2Fmail.google.com%2Fmail%2Fu%2F0%2F%23search%2F%7Bquery%7D) | `gmail` | Gmail search |
| [Google Maps](alfred://customsearch/Google%20Maps/maps/utf8/nospace/https%3A%2F%2Fwww.google.com%2Fmaps%2Fsearch%2F%7Bquery%7D%3Fhl%3Den%26source%3Dopensearch) | `maps` | Google Maps search |
| [Google Translator](alfred://customsearch/Google%20Translator/translate/utf8/nospace/https%3A%2F%2Ftranslate.google.com%2F%3Fsl%3Dauto%26tl%3Den%26text%3D%7Bquery%7D%26op%3Dtranslate) | `translate` | Translates to english from auto-detected language |

### Customize and create new searches

To create or customize your searches, open the Alfred application, and in preferences navigate to **Web Search**.

![](/images/examples/customization-1.png)

There, choose **Add Custom Search** (on the bottom right) to create a new search, or click the gear icon to change existing ones.

To create your new search, just pick the URL for the website you want to search, and replace a search result string on the URL with `{query}`. To add an icon, drag the image into the dialog window.

![](/images/examples/banana-search.png)

![](/images/examples/banana-search-2.png)

Check [Alfred docs](https://www.alfredapp.com/help/features/web-search/) for more details.

### Bookmark Feature

Another noteworthy feature from Alfred is Web Bookmarks, that indexes all the bookmarks on the browser of your choice. This can be used to retrieve frequently opened links just by searching their title or URL. 

**💡 Pro-tip:** Save bookmarks with titles that are easier to remember for searching

![](/images/examples/bookmarks.png)

![](/images/examples/bookmarks.gif)

### Credit and Acknowledgments

The thinking behind this list came from our great handbook page [Searching GitLab like a pro](https://about.gitlab.com/handbook/tools-and-tips/searching/#searching-using-alfred-on-macos)

GitLab team member Simon. M [recorded a video](https://about.gitlab.com/handbook/tools-and-tips/searching/#searching-using-alfred-on-macos) on how to setup this workflow on Alfred and directly on Firefox as well.

Lastly, [Ronald van Zon](https://gitlab.com/rvzon) also maintains a repo [with more advanced Alfred workflows](https://gitlab.com/gitlab-org/alfred) for all things GitLab.
